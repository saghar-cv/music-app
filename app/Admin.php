<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class Admin
 * @package App
 *
 * @property $id
 * @property $name
 * @property $username
 * @property $email
 * @property $password
 */
class Admin extends Authenticatable
{
    use Notifiable;

    const Full_ADMIN = 1;
    const CONTENT_ADMIN = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}
