<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Admin
 * @package Playlist
 *
 * @property integer $id
 * @property string $name
 * @property Date $date
 * @property integer $host_id
 * @property integer $user_id
 * @property integer $privacy
 */
class Playlist extends Model
{
    const PRIVACY_PUBLIC = 0;
    const PRIVACY_PRIVATE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'date', 'host_id', 'user_id', 'privacy'
    ];


    /**
     * Get playlist host
     *
     * @return BelongsTo
     */
    public function getHost()
    {
        return $this->belongsTo('App\Host', 'host_id');
    }

    /**
     * Get playlist user
     *
     * @return BelongsTo
     */
    public function getUser()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Get songs
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getSongs()
    {
        return $this->belongsToMany('App\Song', 'playlist_songs', 'playlist_id', 'song_id');
    }
}
