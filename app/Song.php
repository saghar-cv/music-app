<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Admin
 * @package App
 *
 * @property integer $id
 * @property string $artist
 * @property string $name
 * @property array $genres
 * @property integer $playlist_id
 * @property integer $artist_id
 * @property string $cover_path
 * @property string $music_path
 * @property integer $status
 * @property integer $trend
 * @property \DateTime $date
 */
class Song extends Model
{
    const STATUS_PENDING = 0;
    const STATUS_CONFIRMED = 1;

    const NOT_TREND = 0;
    const TREND = 1;

    protected $fillable = [
        'artist',
        'name',
        'genres',
        'playlist_id',
        'cover_path',
        'music_path',
        'status',
        'date',
    ];

    protected $casts = [
        'genres' => 'array'
    ];

    /**
     * Get playlist
     *
     * @return BelongsTo
     */
    public function getPlaylist()
    {
        return $this->belongsTo('App\Playlist', 'playlist_id');
    }

    /**
     * Get artist
     *
     * @return BelongsTo
     */
    public function getArtist()
    {
        return $this->belongsTo('App\Artist', 'artist_id');
    }

    /**
     * Get playlists
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getPlaylists()
    {
        return $this->belongsToMany('App\Playlist', 'playlist_songs', 'song_id', 'playlist_id');
    }

    /**
     * Get genres as string
     *
     * @return mixed|string
     */
    public function getGenres()
    {
        return implode(",", $this->genres);
    }

    /**
     * Get status label
     *
     * @param $status
     * @return string
     */
    public static function getStatusLabel($status)
    {
        switch ($status) {
            case self::STATUS_CONFIRMED:
                return 'تایید شده';
            case self::STATUS_PENDING:
                return 'در انتظار تایید';
            default:
                return 'تعریف نشده';
        }
    }

    /**
     * Set music trend
     */
    public function setTrend()
    {
        if ($this->trend)
            $this->trend = self::NOT_TREND;
        else
            $this->trend = self::TREND;

        $this->save();
    }
}
