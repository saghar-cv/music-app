<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PlaylistSongs
 * @package App
 *
 * @property integer $id
 * @property integer $playlist_id
 * @property integer $song_id
 */
class PlaylistSongs extends Model
{
    protected $fillable = [
        'playlist_id', 'song_id'
    ];

}
