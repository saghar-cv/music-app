<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PasswordReset
 * @package App
 *
 * @property string $email
 * @property string $token
 */
class UserPasswordReset extends Model
{
    protected $fillable = [
        'email', 'token'
    ];
}
