<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Host
 * @package App
 *
 * @property integer $id
 * @property string $address
 */
class Host extends Model
{
    protected $fillable = [
        'address'
    ];

    /**
     * Get host by address
     *
     * @param $address
     * @return mixed
     */
    public static function getHostByAddress($address)
    {
        return self::where('address', $address)->first();
    }
}
