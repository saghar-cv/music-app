<?php


namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class ArtistsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
//            'avatar_path' => $this->avatar_path,
        ];
    }
}
