<?php


namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class SongResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'artist' => new ArtistsResource($this->getArtist),
            'cover_path' => $this->cover_path,
            'music_path' => $this->cover_path,
            'genres' => $this->genres
        ];
    }
}
