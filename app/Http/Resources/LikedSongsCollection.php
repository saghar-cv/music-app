<?php


namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class LikedSongsCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => LikedSongsResource::collection($this),
            'links' => [
                "first_page_url" => $this->url(1),
                "last_page_url" => $this->url($this->lastPage()),
                "next_page_url" => $this->nextPageUrl(),
                "prev_page_url" => $this->previousPageUrl()
            ],
            "meta" => [
                "current_page" => $this->currentPage(),
                "last_page" => $this->lastPage(),
                "per_page" => $this->perPage(),
                "total" => $this->total()
            ]
        ];
    }
}
