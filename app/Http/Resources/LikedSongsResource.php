<?php


namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class LikedSongsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->song_id,
            'name' => $this->name,
            'artist' => $this->getArtist->name,
            'cover_path' => $this->cover_path,
        ];
    }
}
