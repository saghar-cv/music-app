<?php


namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class EmailMustVerified
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $redirectToRoute
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|void
     */
    public function handle($request, Closure $next, $redirectToRoute = null)
    {
        $user = Auth::guard('api')->user();
        if (!$user ||
            (!$user->hasVerifiedEmail())) {
            return responseApi([],['Your email address is not verified.'],['ایمیل شما تایید نشده است.'],403);
        }

        return $next($request);
    }
}
