<?php

/**
 * @param array $data
 * @param array $errors
 * @param array $messages
 * @param int $code
 * @return \Illuminate\Http\JsonResponse
 */
function responseApi($data = [], $errors = [], $messages = [], $code = 200)
{
    $res = [
        'data' => $data,
        'errors' => $errors,
        'messages' => $messages,
        'code' => $code
    ];
    return response()->json($res, $code);
}

/**
 * @param int $length
 * @return string
 */
function generateRandomString($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
