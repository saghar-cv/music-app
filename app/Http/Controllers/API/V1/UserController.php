<?php


namespace App\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Http\Resources\LikedSongsCollection;
use App\Http\Resources\PlaylistCollection;
use App\Http\Resources\SongsForPlaylistCollection;
use App\LikedSongs;
use App\Playlist;
use App\PlaylistSongs;
use App\Song;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\JWTAuth;

class UserController extends Controller
{
    private $jwt_auth;

    private $user;

    public function __construct(JWTAuth $jwt_auth, User $user)
    {
        $this->jwt_auth = $jwt_auth;
        $this->user = $user;
    }

    /**
     * Get user's liked songs
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function likedSongs()
    {
        if ($user_id = Auth::guard('api')->user()->id) {
            $liked = Song::where('status', Song::STATUS_CONFIRMED)
                ->join('liked_songs', 'liked_songs.song_id', '=', 'songs.id')
                ->where('liked_songs.user_id', $user_id)
                ->paginate(20);

            return responseApi(['songs' => new LikedSongsCollection($liked)]);
        }
        return responseApi([], ['Not found'], ['موزیکی پیدا نشد.'], 404);
    }

    /**
     * Like or dislike songs
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function like(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:songs,id'
        ]);

        if ($validator->fails()) {
            return responseApi([], ['Not found'], ['موزیک مورد نظر پیدا نشد.'], 404);
        }

        if ($isLiked = LikedSongs::where([
            'user_id' => Auth::guard('api')->user()->id,
            'song_id' => $request->id
        ])->first()) {
            try {
                $isLiked->delete();
                return responseApi();
            } catch (\Exception $exception) {
                return responseApi([], [$exception->getMessage()], ['در انجام عملیات خطایی رخ داد.'], $exception->getCode());
            }
        } else {
            try {
                $like = LikedSongs::create([
                    'user_id' => Auth::guard('api')->user()->id,
                    'song_id' => $request->id
                ]);
                return responseApi();
            } catch (\Exception $exception) {
                return responseApi([], [$exception->getMessage()], ['در انجام عملیات خطایی رخ داد.'], $exception->getCode());
            }
        }
    }

    /**
     * Create playlist by user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPlaylist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'privacy' => 'required|integer|in:0,1'
        ]);

        if ($validator->fails()) {
            return responseApi([], ['Unprocessable Entity'], [$validator->getMessageBag()->getMessages()], 422);
        }

        try {
            $playlist = Playlist::create([
                'name' => $request->name,
                'user_id' => Auth::guard('api')->user()->id,
                'privacy' => $request->privacy,
            ]);
            return responseApi(['playlist' => $playlist]);
        } catch (\Exception $exception) {
            return responseApi([], [$exception->getMessage()], ['در انجام عملیات خطایی رخ داد.'], $exception->getCode());
        }
    }

    /**
     * Edit playlist name and privacy by user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editPlaylist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:playlists,id',
            'name' => 'required|string|max:255',
            'privacy' => 'required|integer|in:0,1'
        ]);

        if ($validator->fails()) {
            return responseApi([], ['Unprocessable Entity'], [$validator->getMessageBag()->getMessages()], 422);
        }

        if ($playlist = Playlist::find($request->id)) {
            if (!Auth::guard('api')->user()->id == $playlist->user_id) {
                return responseApi([], ['Access denied'], ['شما برای عملیات مورد نظر مجاز نیستید.'], 403);
            }

            $playlist->name = $request->name;
            $playlist->privacy = $request->privacy;
            try {
                $playlist->save();
                return responseApi(['playlist' => $playlist]);
            } catch (\Exception $exception) {
                return responseApi([], [$exception->getMessage()], ['در انجام عملیات خطایی رخ داد.'], $exception->getCode());
            }
        }

        return responseApi([], ['Not found'], ['پلی لیست مورد نظر یافت نشد.'], 404);
    }

    /**
     * User's playlists
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function playlists()
    {
        if ($playlists = Playlist::where('user_id', Auth::guard('api')->user()->id)->get()) {
            return responseApi(['playlists' => PlaylistCollection::collection($playlists)]);
        }

        return responseApi([], ['Not found'], ['پلی لیستی برای نمایش یافت نشد.'], 404);
    }

    /**
     * Playlist's songs
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function playlistSongs(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:playlists,id',
        ]);

        if ($validator->fails()) {
            return responseApi([], ['Unprocessable Entity'], [$validator->getMessageBag()->getMessages()], 422);
        }

        if ($playlist = Playlist::find($request->id)) {
            if ($playlist->privacy == Playlist::PRIVACY_PRIVATE and
                $playlist->user_id != Auth::guard('api')->user()->id) {
                return responseApi([], ['Access denied'], ['شما برای عملیات مورد نظر مجاز نیستید.'], 403);
            }
        }

        if ($records = PlaylistSongs::where('playlist_id', $request->id)->get()) {
            $song_ids = [];
            foreach ($records as $record) {
                array_push($song_ids, $record->song_id);
            }
            return responseApi(['songs' => SongsForPlaylistCollection::collection(Song::findMany($song_ids))]);
        }

        return responseApi([], ['Not found'], ['موزیکی برای نمایش یافت نشد.'], 404);
    }

    /**
     * Add song to playlist by user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addSongToPlaylist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'song_id' => 'required|integer|exists:songs,id',
            'playlist_id' => 'required|integer|exists:playlists,id',
        ]);

        if ($validator->fails()) {
            return responseApi([], ['Unprocessable Entity'], [$validator->getMessageBag()->getMessages()], 422);
        }

        if ($playlist = Playlist::find($request->playlist_id)) {
            if (!Auth::guard('api')->user()->id == $playlist->user_id) {
                return responseApi([], ['Access denied'], ['شما برای عملیات مورد نظر مجاز نیستید.'], 403);
            }

            if ($in_playlist = PlaylistSongs::where('playlist_id', $request->playlist_id)
                ->where('song_id', $request->song_id)->first()) {
                return responseApi([], ['Unprocessable Entity'], ['موزیک انتخابی در حال حاضر در پلی لیست موجود می‌باشد.'], 422);
            }

            try {
                PlaylistSongs::create([
                    'playlist_id' => $request->playlist_id,
                    'song_id' => $request->song_id,
                ]);
                return responseApi(['playlist' => $playlist]);
            } catch (\Exception $exception) {
                return responseApi([], [$exception->getMessage()], ['در انجام عملیات خطایی رخ داد.'], $exception->getCode());
            }
        }
        return responseApi([], ['Not found'], ['پلی لیست مورد نظر یافت نشد.'], 404);
    }


    /**
     * Remove song from playlist by user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeSongFromPlaylist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'song_id' => 'required|integer|exists:songs,id',
            'playlist_id' => 'required|integer|exists:playlists,id',
        ]);

        if ($validator->fails()) {
            return responseApi([], ['Unprocessable Entity'], [$validator->getMessageBag()->getMessages()], 422);
        }

        if ($playlist = Playlist::find($request->playlist_id)) {
            if (!Auth::guard('api')->user()->id == $playlist->user_id) {
                return responseApi([], ['Access denied'], ['شما برای عملیات مورد نظر مجاز نیستید.'], 403);
            }

            if ($in_playlist = PlaylistSongs::where([
                'song_id' => $request->song_id,
                'playlist_id' => $request->playlist_id
            ])->first()) {
                try {
                    $in_playlist->delete();
                    return responseApi(['playlist' => $playlist]);
                } catch (\Exception $exception) {
                    return responseApi([], [$exception->getMessage()], ['در انجام عملیات خطایی رخ داد.'], $exception->getCode());
                }
            }
            return responseApi([], ['Not found'], ['موزیک مورد نظر در پلی لیست وجود ندارد'], 404);
        }
        return responseApi([], ['Not found'], ['پلی لیست مورد نظر یافت نشد.'], 404);
    }

    /**
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removePlaylist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:playlists,id',
        ]);

        if ($validator->fails()) {
            return responseApi([], ['Unprocessable Entity'], [$validator->getMessageBag()->getMessages()], 422);
        }

        if ($playlist = Playlist::find($request->id)) {
            if (!Auth::guard('api')->user()->id == $playlist->user_id) {
                return responseApi([], ['Access denied'], ['شما برای عملیات مورد نظر مجاز نیستید.'], 403);
            }

            try {
                $playlist->delete();
                return responseApi();
            } catch (\Exception $exception) {
                return responseApi([], [$exception->getMessage()], ['در انجام عملیات خطایی رخ داد.'], $exception->getCode());
            }
        }
        return responseApi([], ['Not found'], ['پلی لیست مورد نظر یافت نشد.'], 404);
    }

}
