<?php

namespace App\Http\Controllers\API\V1\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\PasswordResetRequest;
use App\User;
use App\UserPasswordReset;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    /**
     * Create token password reset
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $request->validate(['email' => 'required', 'string', 'email']);

        $user = User::where('email', $request->email)->first();

        if (!$user)
            return responseApi([], ['Not found'], ['کاربری با این ایمیل یافت نشد.'], 404);

        if (!$user->hasVerifiedEmail())
            return responseApi([], ['Not found'], ['ایمیل وارد شده تایید نشده است.'], 403);

        $passwordReset = UserPasswordReset::updateOrCreate(
            ['email' => $user->email],
            ['email' => $user->email, 'token' => generateRandomString(60)]
        );
        if ($user && $passwordReset) {
            $user->notify(new PasswordResetRequest($passwordReset->token));
            return responseApi([], [], ['ایمیل بازنشانی رمز عبور ارسال شد.']);
        }

        return responseApi([], [], ['سرور در حال حاضر قادر به انجام درخواست ارسالی نمی‌باشد.'], 500);

    }
}
