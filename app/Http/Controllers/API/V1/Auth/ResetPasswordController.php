<?php

namespace App\Http\Controllers\API\V1\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\PasswordResetSuccess;
use App\UserPasswordReset;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return \Illuminate\Http\JsonResponse
     */
    public function find($token)
    {
        $passwordReset = UserPasswordReset::where('token', $token)->first();

        if (!$passwordReset)
            return responseApi([], ['Invalid token'], ['توکن بازنشانی رمز عبور نامعتبر می‌باشد.'], 404);
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return responseApi([], ['Invalid token'], ['توکن بازنشانی رمز عبور نامعتبر می‌باشد.'], 404);
        }
        return responseApi([$passwordReset]);
    }

    /**
     * Reset password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'token' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return responseApi([], ['اطلاعات وارد شده نامعتبر می‌باشد.'], [$validator->errors()], 401);
        }

        $passwordReset = UserPasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();

        if (!$passwordReset)
            return responseApi([], ['Invalid token'], ['توکن بازنشانی رمز عبور نامعتبر می‌باشد.'], 404);

        $user = User::where('email', $passwordReset->email)->first();

        if (!$user)
            return responseApi([], ['Not found'], ['کاربری با این ایمیل یافت نشد.'], 404);

        $user->password = Hash::make($request->password);
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess());
        return responseApi([$user]);
    }
}
