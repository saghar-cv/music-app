<?php

namespace App\Http\Controllers\API\V1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Support\Facades\Auth;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Resend the email verification notification.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function resend()
    {
        $user = Auth::guard('api')->user();
        if ($user->hasVerifiedEmail()) {
            return responseApi([], [], ['ایمیل شما قبلا تایید شده است.'], 422);
        }
        $user->sendEmailVerificationNotification();
        return responseApi([], [], ['تاییدیه ایمیل دوباره ارسال شد.']);
    }
}
