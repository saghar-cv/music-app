<?php

namespace App\Http\Controllers\API\V1\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\JWTAuth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    private $jwt_auth;

    private $user;

    public function __construct(JWTAuth $jwt_auth, User $user)
    {
        $this->jwt_auth = $jwt_auth;
        $this->user = $user;
    }

    /**
     * Handle a login request to the application.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return responseApi([], ['اطلاعات وارد شده نامعتبر می‌باشد.'], [$validator->errors()], 401);
        }

        if ($user = User::validateLogin($request->all())) {
            if ($token = $this->jwt_auth->fromUser($user)) {

                $email_verification_status = $user->hasVerifiedEmail();

                return responseApi([
                    'token' => $token,
                    'email_verification_status' => $email_verification_status,
                    'type' => 'bearer',
                ]);
            }
        }
        return responseApi([], ['نام کاربری یا رمز عبور اشتباه می‌باشد.'], ['Unauthorized'], 401);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api');
    }

    /**
     * Get a validator for an incoming login request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'login_type' => ['required', 'numeric', 'in:0,1'],
            'email' => ['required_if:login_type,==,' . User::LOGIN_TYPE_EMAIL, 'string', 'email'],
            'username' => ['required_if:login_type,==,' . User::LOGIN_TYPE_USERNAME, 'string'],
            'password' => ['required', 'string'],
        ]);
    }
}
