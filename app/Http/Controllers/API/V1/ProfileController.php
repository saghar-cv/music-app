<?php


namespace App\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Notifications\PasswordResetSuccess;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\JWTAuth;

class ProfileController extends Controller
{

    private $jwt_auth;

    private $user;

    public function __construct(JWTAuth $jwt_auth, User $user)
    {
        $this->jwt_auth = $jwt_auth;
        $this->user = $user;
    }

    /**
     * View user's profile
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function view()
    {
        if ($user = Auth::guard('api')->user()) {
            return responseApi($user);
        }

        return responseApi([], ['Not found'], ['درخواست شما معتبر نمی‌باشد.'], 404);
    }

    /**
     * Edit user profile
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return responseApi([], ['اطلاعات وارد شده نامعتبر می‌باشد.'], [$validator->errors()], 401);
        }

        if ($user = $this->user::find(Auth::guard('api')->user()->id)) {
            $user->name = $request->name;
            $user->email = $request->email;
            $user->username = $request->username;
            $user->phone = $request->phone;
            try {
                $user->save();
                return responseApi($user);
            } catch (\Exception $exception) {
                return responseApi([], [$exception->getMessage()], ['در انجام عملیات خطایی رخ داد.'], $exception->getCode());
            }

        }
        return responseApi([], ['Not found'], ['درخواست شما معتبر نمی‌باشد.'], 404);
    }

    /**
     * Edit user's password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        if ($validator->fails()) {
            return responseApi([], ['اطلاعات وارد شده نامعتبر می‌باشد.'], [$validator->errors()], 401);
        }

        if ($user = $this->user::find(Auth::guard('api')->user()->id)) {
            $user->password = Hash::make($request->password);
            try {


                $user->save();
                $user->notify(new PasswordResetSuccess());
                if ($token = $this->jwt_auth->fromUser($user)) {
                    return responseApi([
                        'token' => $token,
                        'type' => 'bearer',
                    ]);
                }
                return responseApi([], ['Invalid token'], ['خطایی رخ داد، لطفا دوباره وارد شوید'], 500);
            } catch (\Exception $exception) {
                return responseApi([], [$exception->getMessage()], ['در انجام عملیات خطایی رخ داد.'], $exception->getCode());
            }
        }
        return responseApi([], ['Not found'], ['درخواست شما معتبر نمی‌باشد.'], 404);
    }

    /**
     * Get a validator for an incoming profile edit request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $user = Auth::guard('api')->user()->id],
            'username' => ['required', 'string', 'max:50', 'unique:users,username,' . $user = Auth::guard('api')->user()->id],
            'phone' => ['string', 'max:11', 'unique:users', 'nullable'],
        ]);
    }


}
