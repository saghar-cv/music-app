<?php


namespace App\Http\Controllers\API\V1;


use App\Artist;
use App\Http\Controllers\Controller;
use App\Http\Resources\ArtistsCollection;
use App\Http\Resources\SongResource;
use App\Http\Resources\SongsCollection;
use App\Http\Resources\SongsResource;
use App\Song;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Home page
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $trends = Song::where('trend', Song::TREND)->where('status', Song::STATUS_CONFIRMED)->get();
        $artists = Artist::paginate(20);

        return responseApi([
            'trends' => SongsResource::collection($trends),
            'artists' => new ArtistsCollection($artists)
        ]);
    }

    /**
     * Get Artist songs
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function artistSongs(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:artists,id'
        ]);

        if ($validator->fails()) {
            return responseApi([], ['Not found'], ['خواننده مورد نظر پیدا نشد.'], 404);
        }

        $songs = Song::where('artist_id', $request->id)->where('status', Song::STATUS_CONFIRMED)->paginate(20);

        return responseApi(['songs' => new SongsCollection($songs)]);
    }

    /**
     * Preview song
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function previewSong(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:songs,id'
        ]);

        if ($validator->fails()) {
            return responseApi([], ['Not found'], ['موزیک مورد نظر پیدا نشد.'], 404);
        }

        return responseApi([
            new SongResource(['song' => Song::find($request->id)])
        ]);
    }

    /**
     * Search artists and songs
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'search' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
            return responseApi([], ['Not found'], ['موزیک مورد نظر پیدا نشد.'], 404);
        }

        $searchString = $request->search;

        $songs = Song::where('status', Song::STATUS_CONFIRMED)
            ->where(function ($query) use ($searchString) {
                $query->where('name', 'like', '%' . $searchString . '%');
            })->paginate(20);

        $artists = Artist::where(function ($query) use ($searchString) {
            $query->where('name', 'like', '%' . $searchString . '%');
        })->paginate(20);

        return responseApi([
            'songs' => new SongsCollection($songs),
            'artists' => new ArtistsCollection($artists)
        ]);
    }

}
