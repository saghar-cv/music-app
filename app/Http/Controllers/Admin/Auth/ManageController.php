<?php


namespace App\Http\Controllers\Admin\Auth;


use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ManageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    /**
     * Edit profile
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function editProfile(Request $request)
    {
        $this->validator($request->all())->validate();
        if ($admin = Admin::find(Auth::guard('web')->user()->id)) {
            $admin->name = $request->name;
            $admin->username = $request->username;
            $admin->email = $request->email;
            if ($request->password != null) {
                $admin->password = Hash::make($request->password);
            }
            try {
                $admin->save();
                Session::flash('message', 'تغییرات با موفقیت زخیره شد.');
                Session::flash('type', 'success');
                return redirect()->to(Route('admin-profile'));
            } catch (\Exception $exception) {
                Session::flash('message', 'در انجام عملیات خطایی رخ داد.');
                Session::flash('type', 'warning');
                return redirect()->to(Route('admin-profile'));
            }
        }
        Session::flash('message', 'اطلاعات پروفایل یافت نشد.');
        Session::flash('type', 'warning');
        return redirect()->to(Route('dashboard'));
    }

    /**
     * Edit admin's profile by full admin
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function edit(Request $request)
    {
        if (!(Auth::guard('web')->user()->full_admin == Admin::Full_ADMIN)) {
            return abort(403);
        }

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:admins,id',
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:admins,username,' . $request->id,
            'email' => 'required|string|email|max:255|unique:admins,email,' . $request->id,
            'full_admin' => 'required|integer|in:0,1',
            'password' => 'nullable|string|min:8|confirmed',
        ]);

        if ($validator->fails()) {
            Session::flash('message', 'اطلاعات وارد شده صحیح نمی‌باشد.');
            Session::flash('type', 'warning');
            return redirect()->back();
        }

        if ($admin = Admin::find($request->id)) {
            $admin->name = $request->name;
            $admin->username = $request->username;
            $admin->email = $request->email;
            $admin->full_admin = $request->full_admin;
            if ($request->password != null) {
                $admin->password = Hash::make($request->password);
            }
            try {
                $admin->save();
                Session::flash('message', 'تغییرات با موفقیت زخیره شد.');
                Session::flash('type', 'success');
                return redirect()->to(Route('admin-details'), [
                    'id' => $admin->id
                ]);
            } catch (\Exception $exception) {
                Session::flash('message', 'در انجام عملیات خطایی رخ داد.');
                Session::flash('type', 'warning');
                return redirect()->back();
            }
        }
        Session::flash('message', 'اطلاعات پروفایل یافت نشد.');
        Session::flash('type', 'warning');
        return redirect()->back();
    }

    /**
     * View admin's profile by full admin
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|void
     */
    public function view(Request $request)
    {
        if (!(Auth::guard('web')->user()->full_admin == Admin::Full_ADMIN)) {
            return abort(403);
        }

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:admins,id',
        ]);

        if ($validator->fails()) {
            Session::flash('message', 'ادمین مورد نظر یافت نشد.');
            Session::flash('type', 'warning');
            return redirect()->to(Route('admin-list'));
        }

        if ($admin = Admin::find($request->id)) {
            return view('auth.view', [
                'admin' => $admin
            ]);
        }
        Session::flash('message', 'صفحه مورد نظر یافت نشد.');
        Session::flash('type', 'warning');
        return redirect()->to(Route('admin-list'));
    }

    /**
     * View profiles's details
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function viewProfile()
    {
        if ($admin = Admin::find(Auth::guard('web')->user()->id)) {
            return view('auth.profile', [
                'admin' => $admin
            ]);
        }
        Session::flash('message', 'صفحه مورد نظر یافت نشد.');
        Session::flash('type', 'warning');
        return redirect()->to(Route('dashboard'));
    }

    /**
     * Delete admin
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function delete(Request $request)
    {
        if (!(Auth::guard('web')->user()->full_admin == Admin::Full_ADMIN)) {
            return abort(403);
        }

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:admins,id',
        ]);

        if ($validator->fails()) {
            Session::flash('message', 'ادمین مورد نظر یافت نشد.');
            Session::flash('type', 'warning');
            return redirect()->to(Route('admin-list'));
        }

        if ($admin = Admin::find($request->id)) {
            try {
                $admin->delete();
                Session::flash('message', 'تغییرات با موفقیت زخیره شد.');
                Session::flash('type', 'success');
                return redirect()->to(Route('admin-list'));
            } catch (\Exception $e) {
                Session::flash('message', 'در انجام عملیات خطایی رخ داد.');
                Session::flash('type', 'warning');
                return redirect()->to(Route('admin-list'));
            }
        }
        Session::flash('message', 'ادمین مورد نظر پیدا نشد.');
        Session::flash('type', 'warning');
        return redirect()->to(Route('admin-list'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected
    function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255', 'unique:admins,username,' . $data['id']],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:admins,email,' . $data['id']],
            'password' => ['nullable', 'string', 'min:8', 'confirmed'],
        ]);
    }

}
