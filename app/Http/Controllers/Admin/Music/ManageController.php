<?php


namespace App\Http\Controllers\Admin\Music;


use App\Http\Controllers\Controller;
use App\Song;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ManageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    /**
     * View music details
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function view(Request $request)
    {
        if ($request->id) {
            if ($song = Song::find($request->id)) {
                return view('music.view', [
                    'song' => $song
                ]);
            }
            Session::flash('message', 'موزیک مورد نظر پیدا نشد.');
            Session::flash('type', 'warning');
            return redirect()->to(Route('music-list'));
        }
        Session::flash('message', 'موزیک انتخاب نشده است.');
        Session::flash('type', 'warning');
        return redirect()->to(Route('music-list'));
    }

    /**
     * Edit
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit(Request $request)
    {
        $this->validator($request->all())->validate();
        if ($request->id) {
            if ($song = Song::find($request->id)) {
                $song->name = $request->name;
                $song->artist_id = $request->artist_id;
                $song->status = $request->status;
                $song->genres = explode(',', $request->genres);
                if ($request->cover) {
                    $file = $request->file('cover');
                    $path = $file->store('public/cover');
                    $song->cover_path = '/storage/app/' . $path;
                }
                if ($request->music) {
                    $file = $request->file('music');
                    $file_name = $file->getClientOriginalName();
                    $song->music_path = '/storage/app/' . $file->storeAs(
                            'public/music',
                            $file_name
                        );
                }
                try {
                    $song->save();
                    return redirect()->to(Route('music-view', ['id' => $song->id]));
                } catch (\Exception $exception) {

                }
            }
            Session::flash('message', 'موزیک مورد نظر پیدا نشد.');
            Session::flash('type', 'warning');
            return redirect()->to(Route('music-list'));
        }
        Session::flash('message', 'موزیک انتخاب نشده است.');
        Session::flash('type', 'warning');
        return redirect()->to(Route('music-list'));
    }

    /**
     * Confirm or cancel publishing music
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatus(Request $request)
    {
        if ($request->id) {
            if ($song = Song::find($request->id)) {
                if ($song->status == Song::STATUS_PENDING) {
                    $song->status = Song::STATUS_CONFIRMED;
                } else {
                    $song->status = Song::STATUS_PENDING;
                }
                try {
                    $song->save();
                    return redirect()->to(Route('music-list'));
                } catch (\Exception $exception) {
                    Session::flash('message', 'در انجام عملیات خطایی رخ داد.');
                    Session::flash('type', 'warning');
                    return redirect()->to(Route('music-list'));
                }
            }
            Session::flash('message', 'موزیک مورد نظر پیدا نشد.');
            Session::flash('type', 'warning');
            return redirect()->to(Route('music-list'));
        }
        Session::flash('message', 'موزیک انتخاب نشده است.');
        Session::flash('type', 'warning');
        return redirect()->to(Route('music-list'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'artist_id' => ['required', 'integer'],
//            'artist' => ['required', 'string', 'max:255'],
            'genre.*' => ['string', 'max:255'],
            'status' => ['required', 'integer', 'in:0,1',],
            'cover' => ['mimes:jpeg,jpg,png'],
            'music' => ['mimetypes:audio/mpeg'],
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function trend(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:songs,id'
        ]);

        if ($validator->fails()) {
            Session::flash('message', 'موزیک مورد نظر یافت نشد');
            Session::flash('type', 'warning');
            return back();
        }
        try {
            $song = Song::find($request->id);
            $song->setTrend();

        } catch (\Exception $exception) {
            Session::flash('message', 'در انجام عملیات خطایی رخ داد.');
            Session::flash('type', 'warning');
        }
        return redirect()->refresh();
    }
}
