<?php


namespace App\Http\Controllers\Admin\Music;


use App\Http\Controllers\Controller;
use App\Song;

class ListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    /**
     * List of admins
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('music.list', ['songs' => Song::orderBy('created_at', 'desc')->simplePaginate(10)]);
    }

}
