<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Host
 * @package App
 *
 * @property integer $id
 * @property string $name
 */
class Artist extends Model
{
    protected $fillable = [
        'name'
    ];

}
