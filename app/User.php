<?php

namespace App;

use App\Notifications\VerifyApiEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 * @package App
 *
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $username
 * @property string $password
 */
class User extends Authenticatable implements MustVerifyEmail, JWTSubject
{
    use Notifiable;

    const LOGIN_TYPE_EMAIL = 0;
    const LOGIN_TYPE_USERNAME = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendApiEmailVerificationNotification()
    {
        $this->notify(new VerifyApiEmail); // my notification
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Validate user and user's password for login
     *
     * @param $data
     * @return User|bool
     */
    public static function validateLogin($data)
    {
        switch ($data['login_type']) {
            case self::LOGIN_TYPE_EMAIL:
                $user = self::where('email', $data['email'])->first();
                break;
            case self::LOGIN_TYPE_USERNAME:
                $user = self::where('username', $data['username'])->first();
                break;
            default:
                return false;
        }

        if ($user) {
            if (Hash::check($data['password'], $user->password)) {
                return $user;
            }
        }
        return false;
    }
}
