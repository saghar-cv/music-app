<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

/**
 * Class LikedSongs
 * @package App
 *
 * @property integer $user_id
 * @property integer $song_id
 */
class LikedSongs extends Model
{
    protected $fillable = [
        'user_id',
        'song_id',
    ];

    /**
     * Get Song
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSong()
    {
        return $this->belongsTo('App\Song', 'song_id');
    }

    /**
     * Get user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getUser()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
