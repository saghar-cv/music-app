## **API Documentation**

## Response Structure
```json
{
  "data": json,
  "errors": json/string,
  "messages": string,
  "code": integer
}
```

## Data With Pagination Structure
```json
{
  "data": {},
  "links": {
    "first_page_url": "",
    "last_page_url": "",
    "next_page_url": "",
    "prev_page_url": ""
  },
  "meta":{
    "current_page":"",
    "last_page": "",
    "per_page": "",
    "total": ""
  }
}
```

## Status Codes

| Status Code | Description |
| :--- | :--- |
| 200 | `OK` |
| 401 | `Unauthorized` |
| 403 | `Access Denied` |
| 404 | `Not Found` |
| 422 | `Unprocessable Entity` |
| 500 | `Internal Server Error` |

## Home
**Home page**
```http
POST /api/v1
```
| Response Data | Description |
| :--- | :--- |
| `trends` | `List of trend musics`|
| `artists` | `List of artists` **Pagination**|

**List of artist's songs**
```http
POST /api/v1/artist-songs
```
| Parameter | Description |
| :--- | :--- |
| `id` | `Artist ID` * **Required**|

| Response Data | Description |
| :--- | :--- |
| `songs` | `List of artist's songs` **Pagination**|

**Preview song**
```http
POST /api/v1/preview-song
```
| Parameter | Description |
| :--- | :--- |
| `id` | `Song ID` * **Required**|

| Response Data | Description |
| :--- | :--- |
| `song` | `Song detail`|

**Search**
```http
POST /api/v1/search
```
| Parameter | Description |
| :--- | :--- |
| `search` | `Search string` * **Required**|

| Response Data | Description |
| :--- | :--- |
| `songs` | `Song result` **Pagination**|
| `artists` | `Artist result` **Pagination**|

## Authentication
**Register**
```http
POST /api/v1/register
```
| Parameter | Description |
| :--- | :--- |
| `name` | `Name` * **Required**|
| `email` | `Email` * **Required**|
| `username` | `Username` * **Required**|
| `phone` | `Phone` |
| `password` | `Password` * **Required**|
| `password-confirmation` | `Password Confirmation` * **Required**|

**Login**
```http
POST /api/v1/login
```
| Parameter | Description |
| :--- | :--- |
| `login_type` | `0 for login by email and 1 for login by username` * **Required**|
| `email` | `Email` * **Required** if login_type = 0|
| `username` | `Username` * **Required** if login_type = 1|
| `password` | `Password` * **Required**|

| Response Data | Description |
| :--- | :--- |
| `token` | `Authentication toke`|
| `email_verification_status` | `Email verification status`|
| `type` | `Token type`|

**Resend verification email**
```http
POST /api/v1/email/resend
```

**Request for new password**
```http
POST /api/v1/password/create
```
| Parameter | Description |
| :--- | :--- |
| `email` | `Email` * **Required**|

**Reset password**
```http
POST /api/v1/password/reset
```
| Parameter | Description |
| :--- | :--- |
| `email` | `Email` * **Required**|
| `token` | `Reset Password token` * **Required**|
| `password` | `Password` * **Required**|
| `password-confirmation` | `Password Confirmation` * **Required**|

## Profile
**View user's profile**
```http
POST /api/v1/profile
```

**Edit profile**
```http
POST /api/v1/edit
```
| Parameter | Description |
| :--- | :--- |
| `name` | `Name` * **Required**|
| `email` | `Email` * **Required**|
| `username` | `Username` * **Required**|
| `phone` | `Phone` |

**Change password**
```http
POST /api/v1/edit-password
```
| Parameter | Description |
| :--- | :--- |
| `password` | `Password` * **Required**|
| `password-confirmation` | `Password Confirmation` * **Required**|

| Response Data | Description |
| :--- | :--- |
| `token` | `Authentication toke`|
| `type` | `Token type`|

## User's actions * email must verified *
**List of user's liked songs**
```http
POST /api/v1/user/liked
```
| Response Data | Description |
| :--- | :--- |
| `songs` | `Liked songs` **Pagination**|

**Like song by user**
```http
POST /api/v1/user/like
```
| Parameter | Description |
| :--- | :--- |
| `id` | `Song ID` * **Required**|

**Create playlist by user**
```http
POST /api/v1/user/playlist/create
```
| Parameter | Description |
| :--- | :--- |
| `name` | `Playlist name` * **Required**|
| `privacy` | `Playlist privacy status - 0 for public 1 for private` * **Required**|

| Response Data | Description |
| :--- | :--- |
| `playlist` | `Playlist detail`|

**Edit playlist by user**
```http
POST /api/v1/user/playlist/edit
```
| Parameter | Description |
| :--- | :--- |
| `id` | `Playlist ID` * **Required**|
| `name` | `Playlist name` * **Required**|
| `privacy` | `Playlist privacy status - 0 for public 1 for private` * **Required**|

| Response Data | Description |
| :--- | :--- |
| `playlist` | `Playlist detail`|

**User's playlists**
```http
POST /api/v1/user/playlist
```
| Response Data | Description |
| :--- | :--- |
| `playlists` | `Playlists`|

**Playlist songs**
```http
POST /api/v1/user/playlist/songs
```
| Parameter | Description |
| :--- | :--- |
| `id` | `Playlist ID` * **Required**|

| Response Data | Description |
| :--- | :--- |
| `songs` | `Playlist songs` **Pagination**|

**Add song to playlist**
```http
POST /api/v1/user/playlist/add-song
```
| Parameter | Description |
| :--- | :--- |
| `song_id` | `Song ID` * **Required**|
| `playlist_id` | `Playlist ID` * **Required**|

| Response Data | Description |
| :--- | :--- |
| `playlist` | `Playlist detail`|

**Remove song form playlist**
```http
POST /api/v1/user/playlist/remove-song
```
| Parameter | Description |
| :--- | :--- |
| `song_id` | `Song ID` * **Required**|
| `playlist_id` | `Playlist ID` * **Required**|

| Response Data | Description |
| :--- | :--- |
| `playlist` | `Playlist detail`|

**Remove playlist**
```http
POST /api/v1/user/playlist/remove
```
| Parameter | Description |
| :--- | :--- |
| `id` | `Playlist ID` * **Required**|


## License
All right reserved to [Geev](https://geevserver.com).
