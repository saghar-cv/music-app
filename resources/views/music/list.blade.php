@extends('layouts.admin')
@section('content')

    @if(\Illuminate\Support\Facades\Session::has('message'))
        <div class="alert alert-{{ \Illuminate\Support\Facades\Session::get('type') }}">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ \Illuminate\Support\Facades\Session::get('message') }}
        </div>
    @endif
    <div class="content" style="padding-top: 100px">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">لیست آهنگها</h3>
            </div>

            <div class="box-body">

                <table class="table table-bordered table-hover table-striped dataTable">
                    <tr>
                        <th>محبوب</th>
                        <th>کاور</th>
                        <th>نام خواننده</th>
                        <th>نام آهنگ</th>
                        <th>ژانر</th>
                        <th>موزیک</th>
                        <th>وضعیت</th>
                        <th>مشاهده</th>
                    </tr>
                    @foreach($songs as $song)
                        <tr>
                            <td>
                                <a href="{{ route('music-trend',['id'=>$song->id]) }}">
                                    @if($song->trend)
                                        <i>is trend</i>
{{--                                        <i class="fas fa-star" style="color: gold"></i>--}}
                                    @else
                                        <i>is not trend</i>
{{--                                        <i class="fas fa-star"></i>--}}
                                    @endif
                                </a>
                            </td>
                            <td><img src="{{url('/') . $song->cover_path}}" height="140" width="140"></td>
                            <td>{{$song->getArtist->name}}</td>
                            <td>{{$song->name}}</td>
                            <td>{{$song->getGenres()}}</td>
                            <td>
                                <audio controls>
                                    <source src="{{url('/') . $song->music_path}}" type="audio/mp3">
                                </audio>
                            </td>
                            @if($song->status == \App\Song::STATUS_PENDING)
                                <td><a href="{{ route('music-change',['id' => $song->id]) }}"
                                       class="waves-effect waves-light btn style-1 ">تایید</a></td>
                            @else
                                <td><a href="{{ route('music-change',['id' => $song->id]) }}"
                                       class="waves-effect waves-light btn style-1 ">لفو</a></td>
                            @endif
                            <td><a href="{{ route('music-view',['id' => $song->id]) }}"
                                   class="waves-effect waves-light btn style-1 ">مشاهده</a></td>
                        </tr>
                    @endforeach
                </table>
                {{$songs->links()}}
            </div>
        </div>

    </div>

@endsection
