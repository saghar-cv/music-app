@extends('layouts.admin')
@section('content')
    <form method="POST" action="{{ route('music-edit', ['id' => $song->id]) }}" enctype="multipart/form-data"
          style="padding-top: 100px">
        @csrf
        <div class="form-group">
            <label for="artist">خواننده</label>
            <select class="form-control @error('artist_id') is-invalid @enderror" name="artist_id">
                @foreach(\App\Artist::all() as $artist)
                <option value="{{$artist->id}}" {{$song->artist_id == $artist->id ? 'selected':''}}>{{$artist->name}}</option>
                @endforeach
            </select>
{{--            <input type="text" class="form-control @error('artist') is-invalid @enderror" name="artist"--}}
{{--                   value="{{$song->artist}}">--}}
            @error('artist_id')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="name">نام آهنگ</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                   value="{{$song->name}}">
            @error('name')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>

        <div class="form-group">
            <label for="genres">ژانر</label>
            <input type="text" class="form-control @error('genres') is-invalid @enderror" name="genres"
                   value="{{$song->getGenres()}}">
            @error('genres')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
            <small id="genreHelp" class="form-text text-muted">ژانرها را با کاما از هم جدا کنید.</small>
        </div>

        <div class="form-group">
            <label for="status">وضعیت</label>
            <select class="form-control @error('status') is-invalid @enderror" name="status">
                <option value="{{\App\Song::STATUS_PENDING}}" {{$song->status == \App\Song::STATUS_PENDING ? 'selected':''}}>{{\App\Song::getStatusLabel(\App\Song::STATUS_PENDING)}}</option>
                <option value="{{\App\Song::STATUS_CONFIRMED}}" {{$song->status == \App\Song::STATUS_CONFIRMED ? 'selected':''}}>{{\App\Song::getStatusLabel(\App\Song::STATUS_CONFIRMED)}}</option>
            </select>
            @error('status')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="cover">کاور</label>
            <img src="{{url('/') . $song->cover_path}}" height="140" width="140">
            <input type="file" class="form-control-file" name="cover" accept="image/png, image/jpeg, image/jpg">
            @error('cover')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="music">موزیک</label>
            <audio controls>
                <source src="{{url('/') . $song->music_path}}" type="audio/mp3">
            </audio>
            <input type="file" class="form-control-file @error('music') is-invalid @enderror" name="music"
                   accept=".mp3, audio/*">
            @error('music')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">ثبت</button>
    </form>

@endsection
