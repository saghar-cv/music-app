@extends('layouts.admin')




@section('content')

    <div class="main-panel">
        <div class="content">

            <div class="page-inner">
                <div class="row rtl mt--1">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card card-stats card-service card-round card-main-page">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-7 col-stats">
                                                <div class="numbers">
                                                    <h4 class="card-title">1,294</h4>
                                                    <p class="card-category">سرویس های فعال</p>

                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="icon-big text-center">
                                                    <i class="fas fa-box"></i>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="card card-stats card-quits card-round card-main-page">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-7 col-stats">
                                                <div class="numbers">
                                                    <h4 class="card-title">1,294</h4>
                                                    <p class="card-category">نقل و قول ها</p>

                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="icon-big text-center">
                                                    <i class="fas fa-paper-plane"></i>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card card-stats card-tickets card-round card-main-page">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-7 col-stats">
                                                <div class="numbers">
                                                    <h4 class="card-title">1,294</h4>
                                                    <p class="card-category">تیکت ها</p>

                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="icon-big text-center">
                                                    <i class="fas fa-ticket-alt"></i>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="card card-stats card-orders card-round card-main-page">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-7 col-stats">
                                                <div class="numbers">
                                                    <h4 class="card-title">1,294</h4>
                                                    <p class="card-category">ٌصورت حساب ها</p>

                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="icon-big text-center">
                                                    <i class="fas fa-money-bill-alt"></i>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100 main-carousel" src="../assets/img/maxresdefault.jpg"
                                         alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100 main-carousel"
                                         src="../assets/img/urban-landscape-flat-design_23-2147506625.jpg"
                                         alt="Second slide">
                                </div>

                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                               data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                               data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row rtl">
                    <div class="col-md-6">
                        <div class="card full-height">
                            <div class="card-header">
                                <div class="card-head-row">
                                    <div class="card-title">فاکتور ها و سررسید ها</div>
                                    <div class="card-tools">
                                        <span class="badge badge-danger float-left">12</span>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-h-o">
                                <table class="table table-striped table-hover table-responsive">
                                    <thead>


                                    </thead>
                                    <tbody>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width">هاست لینوکس 5 گیگ</span></td>
                                        <td><span class="text-success">15000</span>تومان</td>
                                        <td>
                                            <button class="btn btn-secondary btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width">هاست لینوکس 5 گیگ</span></td>
                                        <td><span class="text-success">15000</span>تومان</td>
                                        <td>
                                            <button class="btn btn-secondary btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width">هاست لینوکس 5 گیگ</span></td>
                                        <td><span class="text-success">15000</span>تومان</td>
                                        <td>
                                            <button class="btn btn-secondary btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width">هاست لینوکس 5 گیگ</span></td>
                                        <td><span class="text-success">15000</span>تومان</td>
                                        <td>
                                            <button class="btn btn-secondary btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width">هاست لینوکس 5 گیگ</span></td>
                                        <td><span class="text-success">15000</span>تومان</td>
                                        <td>
                                            <button class="btn btn-secondary btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width">هاست لینوکس 5 گیگ</span></td>
                                        <td><span class="text-success">15000</span>تومان</td>
                                        <td>
                                            <button class="btn btn-secondary btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width">هاست لینوکس 5 گیگ</span></td>
                                        <td><span class="text-success">15000</span>تومان</td>
                                        <td>
                                            <button class="btn btn-secondary btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width">هاست لینوکس 5 گیگ</span></td>
                                        <td><span class="text-success">15000</span>تومان</td>
                                        <td>
                                            <button class="btn btn-secondary btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width">هاست لینوکس 5 گیگ</span></td>
                                        <td><span class="text-success">15000</span>تومان</td>
                                        <td>
                                            <button class="btn btn-secondary btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="card full-height">
                            <div class="card-header">
                                <div class="card-head-row">
                                    <div class="card-title">تیکت های اخیر</div>
                                    <div class="card-tools">
                                        <span class="badge badge-danger float-left">12</span>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-h-o">
                                <table class="table table-striped table-hover">
                                    <thead>

                                    </thead>
                                    <tbody>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width text-secondary">چهارشنبه 8 آبان 98</span>
                                        </td>
                                        <td>فروش و مالی</td>
                                        <td>
                                            <button class="btn btn-success btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width text-secondary">چهارشنبه 8 آبان 98</span>
                                        </td>
                                        <td>فروش و مالی</td>
                                        <td>
                                            <button class="btn btn-danger btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width text-secondary">چهارشنبه 8 آبان 98</span>
                                        </td>
                                        <td>فروش و مالی</td>
                                        <td>
                                            <button class="btn btn-success btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width text-secondary">چهارشنبه 8 آبان 98</span>
                                        </td>
                                        <td>فروش و مالی</td>
                                        <td>
                                            <button class="btn btn-danger btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width text-secondary">چهارشنبه 8 آبان 98</span>
                                        </td>
                                        <td>فروش و مالی</td>
                                        <td>
                                            <button class="btn btn-success btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width text-secondary">چهارشنبه 8 آبان 98</span>
                                        </td>
                                        <td>فروش و مالی</td>
                                        <td>
                                            <button class="btn btn-danger btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width text-secondary">چهارشنبه 8 آبان 98</span>
                                        </td>
                                        <td>فروش و مالی</td>
                                        <td>
                                            <button class="btn btn-success btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width text-secondary">چهارشنبه 8 آبان 98</span>
                                        </td>
                                        <td>فروش و مالی</td>
                                        <td>
                                            <button class="btn btn-danger btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width text-secondary">چهارشنبه 8 آبان 98</span>
                                        </td>
                                        <td>فروش و مالی</td>
                                        <td>
                                            <button class="btn btn-success btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    <tr class="table-cus">
                                        <td><span class="border-left-width text-secondary">چهارشنبه 8 آبان 98</span>
                                        </td>
                                        <td>فروش و مالی</td>
                                        <td>
                                            <button class="btn btn-danger btn-round">پرداخت صورت حساب</button>
                                        </td>

                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                بلاگ
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                گیو دو
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                پلن ها
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright ml-auto">
                    حقوق این وبسایت مطعلق به شرکت می باشد
                </div>
            </div>
        </footer>
    </div>


@endsection
