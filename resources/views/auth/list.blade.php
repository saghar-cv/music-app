@extends('layouts.admin')
@section('content')

    @if(\Illuminate\Support\Facades\Session::has('message'))
        <div class="alert alert-{{ \Illuminate\Support\Facades\Session::get('type') }}">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ \Illuminate\Support\Facades\Session::get('message') }}
        </div>
    @endif
    <div class="content" style="padding-top: 100px">
        <div class="box-header with-border">
            <a href="{{route('register')}}" class="button btn btn-info">ایجاد ادمین جدید</a>

        </div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">همکاران</h3>
            </div>

            <div class="box-body">

                <table class="table table-bordered table-hover table-striped dataTable">
                    <tr>
                        <th>نام</th>
                        <th>نام کاربری</th>
                        <th>ایمیل</th>
                    </tr>
                    @foreach($admins as $admin)
                        <tr>
                            <td>{{$admin->name}}</td>
                            <td>{{$admin->username}}</td>
                            <td>{{$admin->email}}</td>
                            @if(\Illuminate\Support\Facades\Auth::guard('web')->user()->full_admin == \App\Admin::Full_ADMIN)
                                <td>
                                    <a href="{{route('admin-delete', ['id' => $admin->id])}}"
                                       class="btn btn-info">حذف</a>
                                </td>
                                <td>
                                    <a href="{{route('admin-details', ['id' => $admin->id])}}"
                                       class="btn btn-info">ویرایش</a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>

    </div>

@endsection
