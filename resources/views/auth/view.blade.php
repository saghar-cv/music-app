@extends('layouts.admin')

@section('content')
    @if(\Illuminate\Support\Facades\Session::has('message'))
        <div class="alert alert-{{ \Illuminate\Support\Facades\Session::get('type') }}">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ \Illuminate\Support\Facades\Session::get('message') }}
        </div>
    @endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('ویرایش پروفایل') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin-details-edit', ['id' => $admin->id]) }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('نام') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror" name="name"
                                           value="{{ $admin->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="username"
                                       class="col-md-4 col-form-label text-md-right">{{ __('نام کاربری') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="text"
                                           class="form-control @error('username') is-invalid @enderror" name="username"
                                           value="{{ $admin->username }}" required autocomplete="email">

                                    @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-right">{{ __('ایمیل') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror" name="email"
                                           value="{{ $admin->email }}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="full_admin"
                                       class="col-md-4 col-form-label text-md-right">{{ __('نقش') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control @error('full_admin') is-invalid @enderror"
                                            name="full_admin"
                                            required>
                                        <option
                                            value="{{\App\Admin::CONTENT_ADMIN}}" {{$admin->full_admin == \App\Admin::CONTENT_ADMIN ? 'selected': ''}}>
                                            کانتنت منجر
                                        </option>
                                        <option
                                            value="{{\App\Admin::Full_ADMIN}}" {{$admin->full_admin == \App\Admin::Full_ADMIN ? 'selected': ''}}>
                                            فول ادمین
                                        </option>
                                    </select>
                                    @error('full_admin')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password"
                                       class="col-md-4 col-form-label text-md-right">{{ __('رمز جدید') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                           class="form-control @error('password') is-invalid @enderror" name="password"
                                           value="{{null}}" autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm"
                                       class="col-md-4 col-form-label text-md-right">{{ __('تکرار رمز جدید') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" value="{{null}}"
                                           autocomplete="new-password">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('ثبت') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
