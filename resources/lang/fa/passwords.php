<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'رمزعبور باید حداقل هشت حرف داشته باشند و با تکرار گذرواژه‌ مطابقت داشته باشد.',
    'reset' => 'رمزعبور شما دوباره تنظیم شد.',
    'sent' => 'لینک تنظیم مجدد رمزعبور برای شما ارسال شد.',
    'token' => 'توکن بازنشانی رمزعبور نامعتبر است.',
    'user' => "کاربری با این شماره همراه پیدا نشد.",

];
