<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'نام کاربری یا رمزعبور نادرست می‌باشد',
    'throttle' => 'تلاشهای زیادی برای ورود به سیستم صورت گرفته. لطفا :seconds ثانیه دیگر امتحان کنید.',

];
