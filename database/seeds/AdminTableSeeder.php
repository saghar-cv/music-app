<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => 'مدیر کل',
            'username' => 'admin',
            'email' => 'saghar.mojdehi@gmail.com',
            'full_admin' => \App\Admin::Full_ADMIN,
            'password' => \Illuminate\Support\Facades\Hash::make('Geev$123'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
