<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    Route::namespace('API\V1')->group(function () {

        Route::post('', 'HomeController@index')->name('home');
        Route::post('artist-songs', 'HomeController@artistSongs')->name('artist-songs');
        Route::post('preview-song', 'HomeController@previewSong')->name('preview-song');
        Route::post('search', 'HomeController@search')->name('search');

        Auth::routes(['verify' => true]);

        Route::namespace('Auth')->group(function () {
            Route::post('register', 'RegisterController@register')->name('register-api');
            Route::post('login', 'LoginController@login')->name('login-api');
            Route::group(['prefix' => 'password'], function () {
                Route::post('create', 'ForgotPasswordController@create');
                Route::post('reset', 'ResetPasswordController@reset');
            });
            Route::get('email/resend', 'VerificationController@resend')->name('verificationapi.resend');
        });

        Route::middleware('jwt.auth')->group(function () {
            Route::group(['prefix' => 'profile'], function () {
                Route::post('', 'ProfileController@view')->name('user-profile');
                Route::post('edit', 'ProfileController@edit')->name('user-profile-edit');
                Route::post('edit-password', 'ProfileController@editPassword')->name('user-profile-edit-password');
            });

            Route::group(['prefix' => 'user', 'middleware' => 'verify'], function () {
                Route::post('liked', 'UserController@likedSongs')->name('user-liked-songs');
                Route::post('like', 'UserController@like')->name('like-song');
                Route::group(['prefix' => 'playlist'], function () {
                    Route::post('create', 'UserController@createPlaylist')->name('create-playlist');
                    Route::post('edit', 'UserController@editPlaylist')->name('edit-playlist');
                    Route::post('', 'UserController@playlists')->name('playlists');
                    Route::post('songs', 'UserController@playlistSongs')->name('playlist-songs');
                    Route::post('add-song', 'UserController@addSongToPlaylist')->name('add-song-to-playlist');
                    Route::post('remove-song', 'UserController@removeSongFromPlaylist')->name('remove-song-from-playlist');
                    Route::post('remove', 'UserController@removePlaylist')->name('remove-playlist');
                });
            });
        });
    });
});
