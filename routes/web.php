<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Admin')->group(function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::get('', 'HomeController@dashboard')->name('dashboard');
        Route::namespace('Auth')->group(function () {
            Route::get('login', 'LoginController@showLoginForm')->name('login-form');
            Route::post('login', 'LoginController@login')->name('login');
        });
        Route::middleware('auth:web')->group(function () {
            Route::namespace('Auth')->group(function () {
                Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
                Route::post('register', 'RegisterController@register')->name('register-admin');
                Route::get('logout', 'LoginController@logout')->name('logout');
                Route::get('list', 'ListController@index')->name('admin-list');
                Route::get('delete', 'ManageController@delete')->name('admin-delete');
                Route::get('details', 'ManageController@view')->name('admin-details');
                Route::post('edit', 'ManageController@edit')->name('admin-details-edit');
                Route::group(['prefix' => 'profile'], function () {
                    Route::post('edit', 'ManageController@editProfile')->name('admin-profile-edit');
                    Route::get('', 'ManageController@viewProfile')->name('admin-profile');
                });
            });
            Route::namespace('Music')->group(function () {
                Route::group(['prefix' => 'music'], function () {
                    Route::get('list', 'ListController@index')->name('music-list');
                    Route::get('view', 'ManageController@view')->name('music-view');
                    Route::get('change', 'ManageController@changeStatus')->name('music-change');
                    Route::post('edit', 'ManageController@edit')->name('music-edit');
                    Route::get('trend', 'ManageController@trend')->name('music-trend');
                });
            });
        });
    });
});

Route::get('email/verify/{id}', 'VerificationApiController@verify')->name('verificationapi.verify');
Route::namespace('API\V1\Auth')->group(function () {
    Route::get('password/find/{token}', 'ResetPasswordController@find');
});

//Route::get('/home', 'HomeController@index')->name('home');
